#include "OpenRGBSkinPlugin.h"
#include <QHBoxLayout>
#include <QString>
#include <QDir>
#include "SkinApplier.h"
#include "SkinSettings.h"

ResourceManagerInterface* OpenRGBSkinPlugin::RMPointer = nullptr;

OpenRGBPluginInfo OpenRGBSkinPlugin::GetPluginInfo()
{
    printf("[OpenRGBSkinPlugin] Loading plugin info.\n");

    OpenRGBPluginInfo info;
    info.Name         = "Skins plugin";
    info.Description  = "Customize OpenRGB look and feel";
    info.Version  = VERSION_STRING;
    info.Commit  = GIT_COMMIT_ID;
    info.URL  = "https://gitlab.com/OpenRGBDevelopers/openrgbskinplugin";
    info.Icon.load(":/OpenRGBSkinPlugin.png");

    info.Location     =  OPENRGB_PLUGIN_LOCATION_SETTINGS;
    info.Label        =  "Skins";
    info.TabIconString        =  "Skins";
    info.TabIcon.load(":/OpenRGBSkinPlugin.png");

    return info;
}


unsigned int OpenRGBSkinPlugin::GetPluginAPIVersion()
{
    printf("[OpenRGBSkinPlugin] Loading plugin API version.\n");

    return OPENRGB_PLUGIN_API_VERSION;
}


void OpenRGBSkinPlugin::Load(ResourceManagerInterface* resource_manager_ptr)
{
    printf("[OpenRGBSkinPlugin] Loading plugin.\n");
    RMPointer                = resource_manager_ptr;
}


QWidget* OpenRGBSkinPlugin::GetWidget()
{
    printf("[OpenRGBSkinPlugin] Creating widget.\n");

    // Make sure our skins folder is created
    filesystem::path skins_dir = RMPointer->GetConfigurationDirectory() / "plugins" / "skins";

    printf("[OpenRGBSkinPlugin] Creating skins directory.\n");

    bool created = QDir().mkpath(QString::fromStdString(skins_dir.string()));

    if(!created)
    {
        printf("[OpenRGBSkinPlugin] Not able to create the skins folder.\n");
    }

    SkinApplier* skin_applyer = new SkinApplier();

    // Check settings for skin auto load
    json settings = SkinSettings::LoadSettings();

    if(settings.contains("load_at_startup"))
    {
        printf("[OpenRGBSkinPlugin] Auto loading skin.\n");

        std::string skin_file_name = settings["load_at_startup"];
        skin_applyer->SetSkin(skin_file_name);
    }

    return skin_applyer;

}

QMenu* OpenRGBSkinPlugin::GetTrayMenu()
{
    printf("[OpenRGBSkinPlugin] Creating tray menu.\n");

    return nullptr;
}

void OpenRGBSkinPlugin::Unload()
{
    printf("[OpenRGBSkinPlugin] Time to call some cleaning stuff.\n");
}

OpenRGBSkinPlugin::~OpenRGBSkinPlugin()
{
     printf("[OpenRGBSkinPlugin] Time to free some memory.\n");
}


